#!/bin/bash

#colors
RED='\033[0;31m'
PINK='\033[1;35m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

# torlinks
BABUK_LEAKS="http://nq4zyac4ukl4tykmidbzgdlvaboqeqsemkp4t35bzvjeve6zm2lqcjid.onion/#section-3"
CONTI_NEWS="http://continewsnv5otx5kaoje7krkto2qbu3gtqef22mnr7eaxw3y6ncz3ad.onion/"
#LOCK_DATA_AUCTION="http://wm6mbuzipviusuc42kcggzkdpbhuv45sn7olyamy6mcqqked3waslbqd.onion/"

#testing webs
test_web="https://interferencias.tech"
rick_roll="https://media0.giphy.com/media/LXONhtCmN32YU/giphy.gif?cid=790b76118a42808a16878efd6b8e786478ea123c093b674b&rid=giphy.gif&ct=g"

#tags
tag1="interferencias"
tag2="rickroll"

#take testing away when testing is not the case if you want :)
#mkdir "$tag1_$(date)"
#cd "$tag2_$(date)"

#mkdir "testing_$(date)"
#cd "testing_$(date)"

declare -a Onions=($test_web $rick_roll)
declare -a tags=($tag1 $tag2)

len=${#tags[@]}

#debugging
echo $1
count=0		

for (( i=0; i<${len}; ++i )); do
	if [ ${tags[$i]} == $1 ] ;then
		cd modules
	#	mkdir ${tags[$i]}
	#	cd ${tags[$i]}
		echo -e "${PINK}Downloading content from ${tags[$i]}...${NC}"
		torsocks wget --content-disposition "${Onions[$i]}"
		chmod 777 index.html
	fi
done

echo -e "${GREEN}We are done here!${NC}"
cd ..
