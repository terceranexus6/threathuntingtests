import os
import sys

modules = ['babuk','interferencias']    

#get information from tor
#os.system("scripts/tor_grabber.sh")


if len(sys.argv) > 2 :
    first_command = str(sys.argv[1])
    second_command = str(sys.argv[2])
else:
    first_command = str(sys.argv[1])
    second_command = "none"


if first_command == "--help" or first_command == "-h":
    print("usage:")
    print("Check a leak site: ")
    print("feedback.py --ransomware/-r <MYRANSOMWARE>")
    print("Check the modules of leaking sites availables: ")
    print("feedback.py --list/-l")

elif first_command == "--list" or first_command == "-l":
    print("List of Ransomware Leak Sites availables: ")
    # Here would go the modules

elif first_command == "--ransomware" or first_command == "-r":
    if second_command == "none":
        print("ERROR: You need to add a valid second argument")
    else:
        print("Looking for the module "+second_command.lower())
        mypath = "modules/"+second_command.lower()+".py"
    
        if second_command.lower() in modules:
            print("Module found.")
            print("Checking information for "+second_command.lower())
            #chosen_mod = open(path, 'r')
            #commands = chosen_mod.readlines()
            #for line in commands:
                #print(line)
            os.system("python3 "+mypath)

        else:
            print("The module you are looking for doesn exists. Check on --list for available modules or --help for learning more about how to build them.")

else:
    print("ERROR, option not found. Use --help or -h")


