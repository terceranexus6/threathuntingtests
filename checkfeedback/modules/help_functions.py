# function to save part of the text thatś interesting 
def save_part( word1, word2, text ):

    #open the file
    file1 = open(text, 'r')

    #counter for the loop in lines
    count = 0

    #isfound = False

    #where the part of the text is stores
    mylines = []

    #condition to read the part
    save_stuff = False

    while True:
        count += 1

        # Get next line from file
        line = file1.readline()

        # If the first word is found then the condition for storing the part is activated
        if word1 in line:
            save_stuff = True

            #Debugging stuff
            #print("Found first token "+word1+" in the text")
            #print("Line {}: {}".format(count, line.strip()))
            #print(count)

        # While the final word is not found, keep storing
        if save_stuff :
            mylines.append(line)

        # Once is found, stop that
        if word2 in line:
            save_stuff = False

        # if line is empty
        # end of file is reached
        if not line:
            break
    # show the stored part and close file     
    print(mylines)
    file1.close()

    # return the selected part
    return mylines
