import sys
from cryptography.fernet import Fernet

def write_key():
    """
    Generates a key and save it into a file
    """
    key = Fernet.generate_key()
    with open("key.key", "wb") as key_file:
        key_file.write(key)

def load_key():
    """
    Loads the key from the current directory named `key.key`
    """
    return open("key.key", "rb").read()

def encrypt(mess, key):
    """
    Given a filename (str) and key (bytes), it encrypts the file and write it
    """
    f = Fernet(key)
#    mess = message.encode()
    encrypted_data = f.encrypt(mess)
    return encrypted_data


# generate and write a new key
write_key()

# load the previously generated key
key = load_key()

# size of the block
piece_size = 40

with open("test1", "rb") as in_file, open("result", "wb") as out_file:



    # sizing the whole file
    whole = in_file.read()
    size_f = sys.getsizeof(whole)

    # taking the needed size
    piece = in_file.read(piece_size)

    # the part that stays 
    stays = whole[40:size_f]

    if piece == "":
        print ("this is empty")
    
    en = encrypt(piece,key)
#    print(en)
#    print(stays)
    out_file.write(en+stays)

    
print ("done")




