# Threat Hunting Tests

Experiments on threat hunting, mostly on Linux, personal research for career growth.

![](https://media.giphy.com/media/NS7gPxeumewkWDOIxi/giphy.gif)


Feel free to use, improve or point out stuff about the contents as long as it goes accordingly with the LICENCE. 

* Check more about the YARA part [here](https://dev.to/terceranexus6/yara-for-daily-analysis-1o8).
* Check more about the RAKU part [here](https://dev.to/terceranexus6/raku-malware-analysis-jon) and watch my presentation in eslibre [here](eslib.re).
